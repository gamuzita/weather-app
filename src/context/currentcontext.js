import React, { useState, createContext, useContext,useEffect } from "react";

const StoreContext = createContext({});

export const StoreProvider=({children})=>{
    const [histories, setHistory] = useState([]);
    const [searchFh,setSearchFh]= useState('');

    const searchFromHistory=(value)=>{
        setSearchFh(value);
    }

    const addToHistory = (value)=>{
        let currentHistory = localStorage.getItem('historyforecast');
        currentHistory = JSON.parse(currentHistory) || []; 

        //verify if the value already exists in arary
        let exist = false;

        if(currentHistory!==null && currentHistory.length>0){
            const existValue = currentHistory.filter(c=> c.toLowerCase() === value.toLowerCase());

            if(existValue && existValue.length>0){
                exist = true;
            }
        }

        if(!exist)
        {
            currentHistory.push(value);

            if(currentHistory.length>5){
                currentHistory.splice(0,1);
            }

            localStorage.setItem('historyforecast', JSON.stringify(currentHistory));
            setHistory(currentHistory);
        }
    }

    const deleteHistory = (index) =>{
        let currentData = localStorage.getItem('historyforecast');
        currentData = JSON.parse(currentData) || [];

        if(currentData.length>0)
        {
            currentData.splice(index, 1);
            localStorage.setItem('historyforecast', JSON.stringify(currentData));
        }
            
        setHistory(currentData);
    }

    const getFromLocalStorage =()=>{
        let currentHistory = localStorage.getItem('historyforecast') || null;

        if(currentHistory!==null && currentHistory.length>0){
            currentHistory = JSON.parse(currentHistory);
            if(currentHistory.length !== histories.length)
                setHistory(currentHistory);
        }
        else
        {
            if(histories!==null & histories.length>0)
                currentHistory = histories;
            else
                currentHistory = [];
        }

        return currentHistory;
    }

    useEffect(() => {
        getFromLocalStorage();
    }, [histories])

    return (
        <StoreContext.Provider value={{histories, addToHistory, getFromLocalStorage, deleteHistory, searchFromHistory, searchFh}}>
            {children}
        </StoreContext.Provider>
    )

}

export const useStore = () => useContext(StoreContext);