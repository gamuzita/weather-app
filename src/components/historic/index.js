import React, { useEffect} from 'react'
import { useStore } from '../../context/currentcontext';

const Historic = () =>{

    const {histories, deleteHistory, searchFromHistory} = useStore();

    useEffect(() => {
    }, [histories])

    return (
            <table className="table table-hover">
            <thead>
            <tr>
                <th scope="col">Search</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
                {histories.map((ele,i)=>{
                    return <tr className="" key={i}>
                        <th scope="row">{ele}</th>
                        <td><button type="button" className="btn btn-danger" onClick={()=>deleteHistory(i)}>Borrar</button></td>
                        <td><button type="button" className="btn btn-primary" onClick={()=>searchFromHistory(ele)}>Buscar</button></td>
                    </tr>
                })}        
            </tbody>
        </table> 
    )
}

export default Historic
