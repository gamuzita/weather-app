import React from 'react'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'

import "leaflet/dist/leaflet.css";
import './index.css'


const MapPosition =({latitud, longitud})=>{

    const position = [latitud, longitud];

    if(latitud === 0)
    return <div></div>;

    return (
        <div className="col-12 mapdiv">

            <Map center={position} zoom={7} >
                <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                />
                <Marker position={position}>
                    <Popup>
                        A pretty CSS3 popup. <br/> Easily customizable.
                    </Popup>
                </Marker>
           </Map>
        
        </div>
    )
}
export default MapPosition;