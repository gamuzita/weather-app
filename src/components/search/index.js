import React, { useState, useEffect } from "react";
import MapPosition from "../map";
import axios from "axios";
import { useStore } from "../../context/currentcontext";

import "leaflet/dist/leaflet.css";

const Search = () => {
  let initialState = {
    temperature: "",
    pressure: "",
    humidity: "",
    maxtemperature: "",
    mintemperature: "",
    latitud: 51.505,
    longitud: -0.09,
  };
  const { addToHistory, searchFh } = useStore();

  const [record, setRecord] = useState(initialState);
  const [notFound, setNotFound] = useState(false);

  const searchKeyPress = async(e) => {

    let value = e.target.value || "";

      if (e.keyCode===13) {
        e.preventDefault();
        searchForecast(value);
      }


  };

  const searchForecast = async(value) => {
    // e.preventDefault();
    if(value === '')
    return;

    try {
      const result = await axios.get(
        `http://api.openweathermap.org/data/2.5/weather?q=${value}&appid=958c55df4ef45fdb979de381c5529fda`
      );
      
      if(result){
        console.log(result);
          if(result.status === 200){
              const {data} = result;

              const newrecord={
                temperature: data.main.temp,
                pressure: data.main.pressure,
                humidity: data.main.humidity,
                maxtemperature: data.main.temp_max,
                mintemperature: data.main.temp_min,
                latitud: data.coord.lat,
                longitud: data.coord.lon,
              }

              setRecord(newrecord);

              addToHistory(value);

              setNotFound(false);
          }
          else{
            setNotFound(true);
          }
      }
      else{
        setNotFound(true);
      }

    } catch (error) {
      setNotFound(true);
    }
  };

  useEffect(() => {
      searchForecast(searchFh);
  }, [searchFh])


  return (
    <div className="container">
          <div className="row">
            <div className="form-group col-12">
              <label htmlFor="search">Buscar</label>
              <input
                type="text"
                onKeyDown={(e)=>searchKeyPress(e)}
                className="form-control"
                placeholder="Buscar por pais"
              />
            </div>
            {notFound? <div>No information</div>:
            <>
              <div className="form-group col-6">
                <label className="col-form-label">Temperature : {record.temperature}</label>
              </div>
              <div className="form-group col-6">
                <label className="col-form-label">Pressure: {record.pressure}</label>
              </div>
              <div className="form-group col-6">
                <label className="col-form-label">Humidity: {record.humidity}</label>
              </div>
              <div className="form-group col-6">
                <label className="col-form-label">Max temperature: {record.maxtemperature}</label>
              </div>
              <div className="form-group col-6">
                <label className="col-form-label">Min temperature: {record.mintemperature}</label>
              </div>
              <div className="form-group col-6">
                <label className="col-form-label">Latitud: {record.latitud} / Longitud: {record.longitud}</label>
              </div>
              </>
            }
          </div>
          <div className="row">
            <MapPosition
              latitud={record.latitud}
              longitud={record.longitud}
            ></MapPosition>
          </div>
    </div>
  );
};

export default Search;
