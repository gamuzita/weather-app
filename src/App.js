import React from 'react';
import Historic from "./components/historic";
import Search from "./components/search";
import './App.css';
import { StoreProvider } from './context/currentcontext';

const App = () =>{
  return (
    <div className="App">
      <StoreProvider>
        <div className="row">
          <div className="col-6">
          <Historic></Historic>
          </div>
          <div className="col-6">
            <Search></Search>
          </div>
        </div>
      </StoreProvider>
    </div>
  );
}

export default App;
